<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Proveedor</title>
    </head>
    <body>
        <?php
            if(!empty($datos)) {
                $periodo = "";

                $html_table = 
                    '<h2>Proveedor: '.$datos[0]->seller_name.'</h2>
                    <table border="1">
                        <thead>
                            <th>PERIODO</th>
                            <th>ID ACTIVO MAS VENDIDO</th>
                            <th>ID ACTIVO QUE GENERÓ MAS DINERO</th>
                            <th>ID ACTIVO MAS CANCELADO</th>
                        </thead>
                        <tbody>';
                
                $periodo = date("Y-m", strtotime($datos[0]->created));
                $cantidad_max = 0;
                $cantidad_max_cancelado = 0;
                $monto_max = 0;
                $item_id_cantidad = "";
                $item_id_monto = "";
                $item_id_cancelado = "";

                for($i=0; $i<count($datos); $i++) {
                    $periodo = date("Y-m", strtotime($datos[$i]->created));
                    if($cantidad_max < $datos[$i]->item_qty){
                        $item_id_cantidad = $datos[$i]->item_id;
                        $cantidad_max = $datos[$i]->item_qty;
                    }

                    if($datos[$i]->status == "Cancelado"){
                        if($cantidad_max_cancelado < $datos[$i]->item_qty){
                            $item_id_cancelado = $datos[$i]->item_id;
                            $cantidad_max_cancelado = $datos[$i]->item_qty;
                        }
                    }

                    if($monto_max < $datos[$i]->total_sold){
                        $item_id_monto = $datos[$i]->item_id;
                        $monto_max = $datos[$i]->total_sold;
                    }

                    if($periodo != date("Y-m", strtotime($datos[$i+1]->created))) {
                        $html_table .= 
                            '<tr>
                                <td>'.$periodo.'</td>
                                <td>'.$item_id_cantidad.'</td>
                                <td>'.$item_id_monto.'</td>
                                <td>'.$item_id_cancelado.'</td>
                            </tr>';
                        
                        $cantidad_max = 0;
                        $cantidad_max_cancelado = 0;
                        $monto_max = 0;
                        $item_id_cantidad = "";
                        $item_id_monto = "";
                        $item_id_cancelado = "";
                    }
                }

                $html_table .= 
                        '</tbody>
                    </table>';
                
                echo $html_table."<br/>";
            }
        ?>
    </body>
</html>
