<?php
class proveedores_model{
    private $dummy_data_preg_3;
    private $dummy_data_preg_4;
    private $proveedores;
 
    public function __construct(){
        $this->dummy_data_preg_3 = 'https://entrekidscl.s3.amazonaws.com/DummyData.json';
        $this->dummy_data_preg_4 = 'static/DummyData4.json';
        $this->proveedores=array();
    }
    public function getData3(){
        $this->proveedores = json_decode(file_get_contents($this->dummy_data_preg_3));
        
        return $this->proveedores;
    }
    public function getData4($prov_id){
        $this->proveedores = json_decode(file_get_contents($this->dummy_data_preg_4));
        $prov_result = array();

        foreach ($this->proveedores as $key => $value) {
            if($value->seller_id == $prov_id) {
                array_push($prov_result, $value);
            }
        }

        foreach ($prov_result as $key => $value) {
            $aux[$key] = $value->created;
        }
        
        if(isset($aux)) {
            // Ordenamiento por fecha
            array_multisort($aux, SORT_ASC, $prov_result);
        }
        
        return $prov_result;
    }
}
?>
