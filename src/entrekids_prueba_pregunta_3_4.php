<?php
function convertDataToHtml($url){
    $html_result = null;

    $html = 
        '<!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Entrekids Prueba</title>
            </head>
            <body>';

    foreach (json_decode(file_get_contents($url)) as $key => $value) {
        $html_result .= 
            '<tr>
                <td><a target="_blank" href="proveedor.php?id='.$value->seller_id.'">'.$value->seller_name.'</a></td>
                <td>$'.number_format($value->total_sold).'</td>
                <td>'.$value->tipo.'</td>
            </tr>';
    }

    if(is_null($html_result)){
        $html .= 
            '<h3>No existen registros</h3>
            </body>
        </html>';
    } else {
        $html .= 
                '<h2>REGISTROS</h2>
                <table border="1">
                    <thead>
                        <th>PROVEEDOR</th>
                        <th>MONTO TOTAL</th>
                        <th>CATEGORÍA</th>
                    </thead>
                    <tbody>
                        '.$html_result.'
                    </tbody>
                </table>
            </body>
        </html>';
    }

    return $html;
}

$dummy_data_endpoint = 'https://entrekidscl.s3.amazonaws.com/DummyData.json';
$html = convertDataToHtml($dummy_data_endpoint);

// Se crea archivo con extensión html junto al resultado de la dummy consumida.
file_put_contents('resultado_prueba.html', $html);
?>
